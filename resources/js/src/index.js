import Sortable from 'sortablejs';
import axios from 'axios';


class Popup {

    constructor(options) {
        this.searching = false;
        this.options = options;
        this.current_page = 1;
    }

    getModal(){
        return document.getElementById(this.getModalSelector());
    }

    getFooter() {
        return this.getModal().querySelector('.footer');
    }

    static getSelectedItems(){
        return document.querySelectorAll(Popup.getSelectedItemsSelector());
    }

    static getOptions(){
        return this.options;
    }

    getModalSelector() {
        return `${this.options.prefix}my-modal-${this.options.id}`;
    }

    getContentSelectionButton() {
        return document.getElementById(`${this.options.prefix}show-modal-btn-${this.options.id}`);
    }

    getContentSelector() {
        return `${this.options.prefix}show-content-${this.getModal().dataset.productsModals}`;
    }

    getSearchButton(){
        return this.getModal().querySelector('.submit-search');
    }

    getMainContent() {
        return this.getModal().querySelector('.main-content');
    }

    getSearchQueryField(){
        return this.getModal().querySelector('.input-search-field');
    }

    getSearchQuery(){
        return this.getSearchQueryField().value;
    }

    getSearchField(){
        return this.getModal().querySelector('.magento-search-field').value;
    }

    getSearchMessageWrapper(){
        return this.getModal().querySelector('.search-message-wrapper');
    }

    getSearchMessageField(){
        return this.getModal().querySelector('.search-message');
    }

    setSearchMessage(message){
        this.getSearchMessageWrapper().classList.remove('hidden');
        this.getSearchMessageField().classList.remove('hidden');
        this.getSearchMessageField().innerText = message;
    }

    hideSearchMessage(){
        this.getSearchMessageWrapper().classList.add('hidden');
        this.getSearchMessageField().classList.add('hidden');
    }

    getSpinnerField(){
        return this.getModal().querySelector('.spinner');
    }

    getPagination() {
        return this.getModal().querySelector('.pagination');
    }

    getPreviousPageField() {
        return this.getPagination().querySelector('.page-link[data-action="previous"]');
    }

    getNextPageField() {
        return this.getPagination().querySelector('.page-link[data-action="next"]');
    }

    showSpinner(){
        this.getSearchMessageWrapper().classList.remove('hidden');
        this.getMainContent().classList.add('no-scroll');
        this.getSearchMessageWrapper().classList.add('loading');
        this.getSpinnerField().classList.remove('hidden');
        this.getFooter().querySelector('.spinner').classList.remove('hidden');
        this.getPagination().classList.add('hidden');
    }

    hideSpinner(){
        this.getSearchMessageWrapper().classList.add('hidden');
        this.getMainContent().classList.remove('no-scroll');
        this.getSearchMessageWrapper().classList.remove('loading');
        this.getSpinnerField().classList.add('hidden');
        this.getFooter().querySelector('.spinner').classList.add('hidden');
        this.getPagination().classList.remove('hidden');
    }

    bindPaginationEventListeners() {
        const next = this.getNextPageField();
        const previous = this.getPreviousPageField();

        next.addEventListener('click', () => {
            if(! next.classList.contains('disabled') && ! this.searching) {
                this.nextPage();
            }
        });
        previous.addEventListener('click', () => {
            if(! previous.classList.contains('disabled') && ! this.searching) {
                this.previousPage();
            }
        });
    }

    static getSelectedItemsSelector(){
        return '[data-products]';
    }

    search() {
        this.hideSearchMessage();
        this.current_page = 1;

        if(this.getSearchField()) {
            return this.queryEndpoint().then(() => {
                this.bindPaginationEventListeners();
            });
        }

        return Promise.reject();
    }

    previousPage() {
        this.current_page -= 1;
        this.queryEndpoint().then(() => {
            this.bindPaginationEventListeners();
            this.checkBoxes();
        });
    }

    nextPage() {
        this.current_page += 1;
        this.queryEndpoint().then(() => {
            this.bindPaginationEventListeners();
            this.checkBoxes();
        });
    }

    checkBoxes(){

        const content = document.getElementById(this.getContentSelector());
        const selectedValues = Array.from(content.querySelectorAll('li')).map(function(item) { return item.dataset.sku; });
        const checkboxes = this.getModal().querySelectorAll('td input[type="checkbox"]');

        checkboxes.forEach(function iterator(checkbox) {
            if (selectedValues.includes(checkbox.value)) {
                checkbox.checked = true;
            } else {
                checkbox.checked = false;
            }
        })
    }

    /* searchField, searchQuery, modal,selector,contentSelector */

    queryEndpoint(){

        const actionUrl = window.globals.queryActionUrl;
        const modal = this.getModal();
        const parentThis = this;

        this.showSpinner();
        this.searching = true;

        return axios.get(actionUrl, {
            params : {
                'fieldName' : this.getSearchField(),
                'fieldValue' : this.getSearchQuery(),
                'currentPage' : this.current_page,
                [window.globals.querySiteParam] : window.globals.currentSite,
                [window.globals.csrfTokenName] : window.globals.csrfTokenValue
            }
        })
            .then(function (response) {

                const json = response.data;
                const table = modal.querySelector('.products-table');
                const pagination = parentThis.getPagination();
                const fieldId = parentThis.options.namespacedId;
                const fieldInfo = window[fieldId];

                window[fieldId].products = fieldInfo.products.concat(json.products);
                table.innerHTML = json.templates.products;
                pagination.innerHTML = json.templates.pagination;

                parentThis.saveSelection();
            })
            .catch(function (error) {
                console.log(error);
                parentThis.setSearchMessage('Er is een fout opgetreden, probeer het later opnieuw.');
            })
            .finally(() => {
                this.searching = false;
                parentThis.hideSpinner();
            });
    }

    saveSelection(){

        const modal = this.getModal();
        const checkboxes = modal.querySelectorAll('td input');
        const select = modal.querySelector('div.right div.submit');
        const parentThis = this;

        /* prefix off field needs to be here aswell!
        example fields-contentBlocks-blocks-new1-fields-show-content-products */

        const content = document.getElementById(this.getContentSelector());

        select.addEventListener ('click', function modalValidate() {

            const oldContent = [];
            const allli = content.getElementsByTagName("li");

            allli.forEach(function test (li) {
                oldContent.push(li.dataset.sku);
            });

            let count = 0;

            checkboxes.forEach(function iterator(checkbox) {

                /* eslint-disable */
                if(checkbox.checked){

                    // if not in list -> add it to dom
                    if (oldContent.indexOf(checkbox.value) === -1) {

                        const fieldId = parentThis.options.namespacedId;
                        const fieldInfo = window[fieldId];
                        const inputName = fieldInfo.inputNamespaced;

                        const products = fieldInfo.products;
                        const item = products.find(product => product.sku === checkbox.value);

                        const li  = `<li data-sku="${item.sku}" class="element large hasthumb removable">

                        <div class="elementthumb">
                            <img src="${item.image}" style="height: 100%; width: auto;"/>
                        </div>
                        <a class="delete icon selectedContent" title="Remove"></a>
                        <input type="hidden" name="${inputName}[]" value="${checkbox.value}">
                        <div class="label">
                            <span class="title">
                              ${item.sku} -  ${item.name}
                            </span>
                        </div>
                    </li>`;

                        content.insertAdjacentHTML('beforeend', li);
                    }

                    // if there was no content and there is now remove the parent empty span
                    Array.from(content.children).forEach(child => {
                        if(child.querySelector('span').innerText === '') {
                            child.remove();
                        }
                    });

                    // count if there is content
                    count = +1


                } else {
                    // if something is unchecked remove it
                    if(oldContent.indexOf(checkbox.value) !== -1) {
                        content.querySelector('[data-sku="' + checkbox.value + '"').remove();
                    }
                }

            });

            /* eslint-enable */
            if(count === 0){

                Popup.insertEmptyLi(parentThis.options,modal.dataset.productsModals,content);
            }

            parentThis.deleteItems();

        });
    }

    contentManipulation(){

        const selectedItems = Popup.getSelectedItems();

        Array.from(selectedItems).forEach(selectedItem => {

            const items = document.getElementById(`${this.options.prefix}show-content-${selectedItem.dataset.products}`);

            if(items) {
                /* eslint-disable */
                const sortable = Sortable.create(items);
                /* eslint-enable */
            }
        });
    }


    static insertEmptyLi(options,fieldName,content) {

        const inputName = options.inputNamespaced;

        console.log(inputName);

        const html = `<li data-sku=""><input type="hidden" name="${inputName}[]" value=""><span></span></li>`;
        content.insertAdjacentHTML('beforeend', html);
    }

    deleteItems(){

        const content = document.getElementById(this.getContentSelector());
        const deleteIcons = content.querySelectorAll(`a.selectedContent`);
        const parentThis = this;

        deleteIcons.forEach(deleteIcon => {

            deleteIcon.addEventListener ('click', function deleteItem() {

                const list = deleteIcon.parentNode.parentNode;
                deleteIcon.parentNode.remove();

                if(list && list.children.length === 0) {

                    Popup.insertEmptyLi(parentThis.options,list.dataset.products,list);
                }
            });
        });
    }


    init(){

        // uncheck boxes if they were removed in the preview
        const button = this.getContentSelectionButton();
        button.addEventListener('click', () => {
           this.checkBoxes();
        });

        const modal = this.getModal();
        if(modal) {

            // If the enter-key event on the search field is registered we start the search
            const searchField = this.getSearchQueryField();
            searchField.addEventListener('keyup', (event) => {
                const key = event.key || event.keyCode;

                if(key === 'Enter' || key === 13) {
                    this.search();
                }
            });

            // If the search button is clicked we start the search
            const searchButton = this.getSearchButton();
            searchButton.addEventListener('click', () => {
                this.search();
            });

            // If a checkbox gets checked this gets triggerd
            this.saveSelection();

            // If modal opens check the checkboxes that need to be checked
            this.checkBoxes();
            Popup.getSelectedItems();

            this.contentManipulation();
            this.deleteItems();
        }
    }
}

Window.prototype.magentoPopup = function(options){

    const popup = new Popup(options);
    popup.init();

};
