# MagentoProductRelation plugin for Craft CMS 3.x

Plugin to show content from Magento into Craft pages

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.41 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /magento-product-relation

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for MagentoProductRelation.

## Configuring MagentoProductRelation

### Settings plugin

This plugin requires next settings fields to be filled in:

* The endpoint
* The URL
* Authorization / Access token

## Using MagentoProductRelation

### 1. Add the content that you want

Once you assigned the field type to a section/entry you can start adding content.
Just by clicking the button: "Choose content" a modal pops up and you can check the content you want on your page.

### 2. Use in your twig templates

By adding this line of code:

```twig
{% set productsMagento = magentoContent(entry.magento) %}
```
You can loop over the magentoProducts like this:

```twig
{% for product in productsMagento %}
    <ul>
        <li>Product number {{ product.sku }}</li>
        <li>Name: {{ product.name }}</li>
        <li>Price € {{product.price }}</li>
        <p>de URL: {{ product.url }}</p>
        <li>image: <img src="{{ product.image }}" alt="{{product.name }}" width="250px"></li>
        
        {{ product.isNew }}, (bool)
        {{ product.isDiscounted }}, (bool)
        {{ product.discountPrice }}, (float)
        {{ product.discountPercentage }} (string)
        
    </ul>
{% endfor %}

```

Brought to you by [Glue](https://www.glue.be)
