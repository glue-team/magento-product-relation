<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

namespace glue\magentoproductrelation;

use Craft;
use craft\base\Plugin;
use craft\events\PluginEvent;
use craft\events\RegisterComponentTypesEvent;
use craft\services\Fields;
use craft\services\Plugins;
use glue\magentoproductrelation\Exceptions\RequestException;
use glue\magentoproductrelation\Exceptions\UnauthorizedException;
use glue\magentoproductrelation\fields\MagentoProductRelationField as MagentoProductRelationFieldField;
use glue\magentoproductrelation\models\Settings;
use glue\magentoproductrelation\services\magento\model\ProductInterface;
use glue\magentoproductrelation\services\magento\model\StoreView as DataStoreView;
use glue\magentoproductrelation\services\magento\rest\QueryInterface;
use glue\magentoproductrelation\services\magento\rest\stores\StoreView;
use glue\magentoproductrelation\services\magento\SearchFactory;
use glue\magentoproductrelation\services\magento\SearchSelector;
use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\transformers\ProductTransformerInterface;
use glue\magentoproductrelation\twigextensions\MagentoProductRelationTwigExtension;
use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Glue
 * @package   MagentoProductRelation
 * @since     1.0.2
 *
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class MagentoProductRelation extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * MagentoProductRelation::$plugin
     *
     * @var MagentoProductRelation
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '1.0.2';


    public $overrides = [];

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * MagentoProductRelation::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;
        // Add in our Twig extensions
        Craft::$app->view->registerTwigExtension(new MagentoProductRelationTwigExtension());

        // Craft::$app->view->hook('webshop-content', function(array &$context) {
        //     $context['foo'] = 'foo';
        //     include('FilterSelectedSkus.php');
        //     var_dump($context['foo']);
        //     return '<p>Here comes the webshopContent</p>';
        // });

        $this->setCustomClasses();

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = MagentoProductRelationFieldField::class;
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        /**
         * Logging in Craft involves using one of the following methods:
         *
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
         *
         * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
         * the category to the method (prefixed with the fully qualified class name) where the constant appears.
         *
         * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
         * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
         *
         * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
         */
        Craft::info(
            Craft::t(
                'magento-product-relation',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    private function setCustomClasses(){

        $config = Craft::$app->config->getConfigFromFile('magento-product-relation');

        $serviceTypes = InterfaceType::all();

        $interfaces = [
            InterfaceType::REST  =>  [
                'product'       =>  ProductInterface::class,
                'transformer'   =>  ProductTransformerInterface::class,
                'query'         =>  QueryInterface::class,
            ],
            InterfaceType::GRAPH_QL   =>  [
                'product'       =>  ProductInterface::class,
                'transformer'   =>  ProductTransformerInterface::class,
                'query'         =>  \glue\magentoproductrelation\services\magento\graphql\Query::class,
            ]
        ];

        foreach ($serviceTypes as $serviceType){

            if(array_key_exists($serviceType,$config)){

                $custom = $config[$serviceType];

                foreach ($interfaces[$serviceType] as $key=>$interface){

                    if(isset($custom[$key])){

                        $this->checkInterfaceImplemented($custom[$key],$serviceType,$key,$interface);
                    }

                }
            }
        }
    }

    private function checkInterfaceImplemented($class,$serviceType,$key,$interface){

        $interfaceImplementations = class_implements($class);

        if(isset($interfaceImplementations[$interface])){

            $this->overrides[$serviceType][$key] = $class;

        }

    }

    public function getOverrideByTypeAndKey($serviceType,$configKey){

        if(is_array($this->overrides) && array_key_exists($serviceType,$this->overrides) && array_key_exists($configKey,$this->overrides[$serviceType])){

            return $this->overrides[$serviceType][$configKey];
        }

        return null;
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {
        $variables = [
            'settings' => $this->getSettings(),
            'sites'    => $this->getSettings()->getSites(),
        ];

        try {
            $variables['magentoStoreViews'] = $this->getSettings()->getMagentoStoreViews();
        } catch(UnauthorizedException $e) {
            if(empty($this->getSettings()->getErrors('authToken'))) {
                $this->getSettings()->addError('authToken', 'Invalid authorization token');
            }
        } catch (\Exception $e) {

        }

        return Craft::$app->view->renderTemplate(
            'magento-product-relation/settings',
            $variables
        );
    }
}
