<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

/**
 * MagentoProductRelation config.php
 *
 * This file exists only as a template for the MagentoProductRelation settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'magento-product-relation.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    "cacheDuration" =>  3600,

    "rest" => [
        'product'   =>  null,
        'transformer'   =>  null,
        'query' =>  null,
    ],
    "graphQl"   =>  [
        'product'   =>  null,
        'transformer'   =>  null,
        'query' =>  null,
    ]

];
