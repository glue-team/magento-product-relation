<?php

namespace glue\magentoproductrelation\Exceptions;

use Throwable;

class StoreViewNotFoundException extends MagentoProductRelationException
{
    //
}
