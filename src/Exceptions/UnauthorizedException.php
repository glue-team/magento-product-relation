<?php

namespace glue\magentoproductrelation\Exceptions;

class UnauthorizedException extends RequestException
{
    public function __construct($message = 'Invalid authentication token', $code = 0, \Exception $previous = null)
    {
        parent::__construct(401, $message, $code, $previous);
    }
}
