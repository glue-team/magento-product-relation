<?php

namespace glue\magentoproductrelation\Exceptions;

class ConnectionException extends RequestException
{
    public function __construct($message = 'Could not connect to the server', $code = 0, \Exception $previous = null)
    {
        parent::__construct(0, $message, $code, $previous);
    }
}
