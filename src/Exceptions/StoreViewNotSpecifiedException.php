<?php

namespace glue\magentoproductrelation\Exceptions;

use Throwable;

class StoreViewNotSpecifiedException extends MagentoProductRelationException
{
    //
}
