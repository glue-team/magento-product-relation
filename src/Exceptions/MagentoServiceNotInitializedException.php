<?php

namespace glue\magentoproductrelation\Exceptions;

class MagentoServiceNotInitializedException extends MagentoProductRelationException
{
    //
}
