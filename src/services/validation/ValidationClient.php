<?php

namespace glue\magentoproductrelation\services\validation;

use glue\magentoproductrelation\Exceptions\UnauthorizedException;
use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\rest\Client as RestClient;
use GuzzleHttp\Client;

class ValidationClient extends MagentoClient
{

    public function __construct($url, $token)
    {
        $client = new Client([
            'headers'  => [
                'accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'base_uri' => $url,
        ]);

        $this->setProductUri(RestClient::REST_PREFIX . '/' . RestClient::REST_VERSION . '/' . 'store/storeViews');

        parent::__construct($client);
    }

    public function authorized(): bool
    {
        try {
            $this->endpointRequest();
        } catch(UnauthorizedException $e) {
            return false;
        }

        return true;
    }

    public function unauthorized(): bool
    {
        return ! $this->authorized();
    }
}
