<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:35
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\model\StoreView;

interface SearchFactory {

    public function __construct(StoreView $storeView);

    public function createSearch() : Search;
}
