<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:45
 */

namespace glue\magentoproductrelation\services\magento;


class SearchService
{
    private $search;

    public function __construct(SearchFactory $search)
    {
        $this->search = $search;
    }

    public function sku(array $skus, array $options = []) : array {

        return $this->search->createSearch()->sku($skus, $options);
    }

    public function name(string $name, array $options = []) : array {

        return $this->search->createSearch()->name($name, $options);
    }
}
