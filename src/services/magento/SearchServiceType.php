<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 15:49
 */

namespace glue\magentoproductrelation\services\magento;


final class SearchServiceType
{
    const REST = 0;
    const GRAPH_QL = 1;
}
