<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:08
 */

namespace glue\magentoproductrelation\services\magento\settings;


class InterfaceTypeVariant
{
    const QUERY = 'query';
    const TRANSFORMER = 'transformer';
    const PRODUCT = 'product';

    public static function all() : array {

        return [
            self::QUERY,
            self::TRANSFORMER,
            self::PRODUCT,
        ];
    }
}
