<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:05
 */

namespace glue\magentoproductrelation\services\magento\settings\searchcriteria;

abstract class Builder
{

    protected $fields;

    public function addField($field): self
    {
        $this->fields[] = $field;

        return $this;
    }

    abstract public function toArray(): array;
}
