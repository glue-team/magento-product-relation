<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:18
 */

namespace glue\magentoproductrelation\services\magento\settings\searchcriteria;


final class Condition
{
    const IN = 'in';
    const EQUALS = 'eq';
    const LIKE = 'like';
}
