<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:02
 */

namespace glue\magentoproductrelation\services\magento\settings\searchcriteria;

class Filter
{

    protected $field;

    protected $value;

    protected $condition;

    public function __construct($field, $value, $condition)
    {
        $this->field = $field;
        $this->value = $value;
        $this->condition = $condition;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getCondition(): string
    {
        return $this->condition;
    }

    public function toArray()
    {

        return [
            'field'          => $this->field,
            'value'          => $this->value,
            'condition_type' => $this->condition,
        ];
    }
}
