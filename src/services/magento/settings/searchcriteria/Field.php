<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:20
 */

namespace glue\magentoproductrelation\services\magento\settings\searchcriteria;


class Field
{
    const SKU = 'sku';
    const ID = 'id';
    const NAME = 'name';
    const PRICE = 'price';
    const TYPE_ID = 'type_id';
    const CHILDREN = 'children';

    const CUSTOM_ATTRIBUTES = 'custom_attributes';

    const VISIBILITY = 'visibility';

}
