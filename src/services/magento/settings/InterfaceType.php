<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:08
 */

namespace glue\magentoproductrelation\services\magento\settings;


class InterfaceType
{
    const REST = 'rest';
    const GRAPH_QL = 'graphQl';

    public static function all() : array {

        return [
            self::REST,
            self::GRAPH_QL,
        ];
    }
}
