<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 13:33
 */

namespace glue\magentoproductrelation\services\magento\settings;


final class AttributeCode
{
    //@todo list all the other options!

    const IMAGE = 'image';
    const URL_KEY = 'url_key';

    const DISCOUNT_PRICE = 'discount_price';
    const RELEASE_DATE = 'release_date';

}
