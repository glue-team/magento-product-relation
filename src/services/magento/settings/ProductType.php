<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 13:55
 */

namespace glue\magentoproductrelation\services\magento\settings;

class ProductType
{
    const SIMPLE = 'simple';
    const CONFIGURABLE = 'configurable';
}
