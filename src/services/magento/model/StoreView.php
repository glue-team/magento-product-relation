<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:48
 */

namespace glue\magentoproductrelation\services\magento\model;

class StoreView extends AbstractStoreView
{
    private $storeView;

    public function __construct(array $storeView)
    {
        $this->storeView = $storeView;
    }

    public function getIsActive(): bool
    {
        return isset($this->storeView['is_active']) ? (bool)$this->storeView['is_active'] : false ;
    }

    public function getStoreGroupId(): int
    {
        return (int)$this->storeView['store_group_id'];
    }

    public function getWebsiteId(): int
    {
        return (int)$this->storeView['website_id'];
    }

    public function getName(): string
    {
        return $this->storeView['name'];
    }

    public function getCode(): string
    {
        return $this->storeView['code'];
    }

    public function getId(): int
    {
        return (int)$this->storeView['id'];
    }


}
