<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:48
 */

namespace glue\magentoproductrelation\services\magento\model;

interface ProductInterface {

    public function name() : string;

    public function sku() : string;

    public function id() : int;

    public function imageUrl() : string;

    public function url() : string;

    public function price() : string;

    public function getName() : string;

    public function getSku() : string;

    public function getId() : int;

    public function getImageUrl() : string;

    public function getUrl() : string;

    public function getPrice();

    public function getDiscountPrice() : float;

    public function discountPrice() : float;

    public function discountPercentage() : string;

    public function getDiscountPercentage() : string;

    public function isNew() : bool;

    public function getIsNew() : bool;

    public function isDiscounted() : bool;

    public function getIsDiscounted() : bool;
}
