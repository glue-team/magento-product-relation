<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:48
 */

namespace glue\magentoproductrelation\services\magento\model;

abstract class Product implements ProductInterface
{
    protected $name;
    protected $sku;
    protected $url;
    protected $imageUrl;
    protected $price;
    protected $id;

    public function sku(): string
    {
        return $this->getSku();
    }

    public function id(): int
    {
        return $this->getId();
    }

    public function url(): string
    {
        return $this->getUrl();
    }

    public function imageUrl(): string
    {
        return $this->getImageUrl();
    }

    public function name(): string
    {
        return $this->getName();
    }

    public function price(): string
    {
        return $this->getPrice();
    }

    public function isDiscounted(): bool
    {
        return $this->getIsDiscounted();
    }

    public function discountPrice() : float
    {
        return $this->getDiscountPrice();
    }

    public function discountPercentage(): string
    {
        return $this->getDiscountPercentage();
    }

    public function isNew(): bool
    {
        return $this->getIsNew();
    }

}
