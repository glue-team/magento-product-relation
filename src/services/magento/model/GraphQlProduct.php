<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 11:22
 */

namespace glue\magentoproductrelation\services\magento\model;


class GraphQlProduct extends Product
{
    private $product;

    public function __construct(array $product)
    {
        $this->product = $product;
    }

    public function getSku(): string
    {
        return $this->product['sku'];
    }

    public function getId(): int
    {
        return (int)$this->product['id'];
    }

    public function getUrl(): string
    {
        return $this->product['canonical_url'] ?? '';
    }

    public function getImageUrl(): string
    {
        return $this->product['image']['url'];
    }

    public function getName(): string
    {
        return $this->product['name'];
    }

    public function getPrice(): float
    {
        return (float)$this->product['price']['regularPrice']['amount']['value'];
    }

    public function getIsDiscount() : bool {

        //@todo implement
        return false;
    }

    public function getDiscountPrice() : float {

        //@todo implement when more time..
        return (float)0;
    }

    public function getDiscountPercentage() : string {

        //@todo implement when more time..
        return (int)0;
    }

    public function getIsNew() : bool {

        //@todo implement when more time..
        return false;
    }

    public function getIsDiscounted(): bool
    {
        // TODO: Implement getIsDiscounted() method.
        return false;
    }
}
