<?php

namespace glue\magentoproductrelation\services\magento\model;

abstract class AbstractStoreView implements StoreViewInterface
{
    protected $id;
    protected $code;
    protected $name;
    protected $websiteId;
    protected $storeGroupId;
    protected $isActive;

    public function id(): int
    {
        return $this->getId();
    }

    public function code(): string
    {
        return $this->getCode();
    }

    public function name(): string
    {
        return $this->getName();
    }

    public function websiteId(): int
    {
        return $this->getWebsiteId();
    }

    public function storeGroupId(): int
    {
        return $this->getStoreGroupId();
    }

    public function isActive(): bool
    {
        return $this->getIsActive();
    }

}
