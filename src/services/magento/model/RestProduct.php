<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 11:22
 */

namespace glue\magentoproductrelation\services\magento\model;

use glue\magentoproductrelation\services\magento\settings\AttributeCode;
use glue\magentoproductrelation\MagentoProductRelation;

class RestProduct extends Product
{
    use AttributeTrait;

    protected $product;

    public function __construct(array $product)
    {
        $this->product = $product;
    }

    public function getSku(): string
    {
        return $this->product['sku'];
    }

    public function getId(): int
    {
        return (int)$this->product['id'];
    }

    public function getUrl(): string
    {
        $urlKey = $this->filterAttribute($this->product['custom_attributes'],AttributeCode::URL_KEY);

        return rtrim(MagentoProductRelation::getInstance()->getSettings()->url,'/') . '/' . $urlKey . '.html';
    }

    public function getImageUrl(): string
    {
        $imageUrl = $this->filterAttribute($this->product['custom_attributes'],AttributeCode::IMAGE);

        $mediaUrl = MagentoProductRelation::getInstance()->getSettings()->url;

        if(MagentoProductRelation::getInstance()->getSettings()->media_url != ''){
            $mediaUrl = MagentoProductRelation::getInstance()->getSettings()->media_url;
        }

        return $mediaUrl . 'media/catalog/product'. $imageUrl;
    }

    public function getIsDiscounted() : bool {

        return $this->isProductDiscounted($this->getPrice(),$this->getDiscountPrice());
    }

    public function getDiscountPrice() : float
    {
        return isset($this->product['discount_price']) ? (float)$this->product['discount_price'] : (float)0;
    }

    public function getDiscountPercentage() : string {

        return $this->getDiscountPercent($this->getDiscountPrice(),$this->getPrice());
    }

    public function getIsNew() : bool {

        $releaseDate = $this->filterAttribute($this->product['custom_attributes'],AttributeCode::RELEASE_DATE);

        return $this->isNewDate($releaseDate);
    }

    public function getName(): string
    {
        return isset($this->product['name']) ? $this->product['name'] : '';
    }

    public function getPrice()
    {
        return (isset($this->product['price'])) ? (float) $this->product['price'] : '';
    }
}
