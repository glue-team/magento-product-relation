<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:48
 */

namespace glue\magentoproductrelation\services\magento\model;

interface StoreViewInterface {

    public function id() : int;

    public function getId() : int;

    public function code() : string;

    public function getCode() : string;

    public function name() : string;

    public function getName() : string;

    public function websiteId() : int;

    public function getWebsiteId() : int;

    public function storeGroupId() : int;

    public function getStoreGroupId() : int;

    public function isActive() : bool;

    public function getIsActive() : bool;
}
