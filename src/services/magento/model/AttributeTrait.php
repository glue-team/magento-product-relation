<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 11:49
 */

namespace glue\magentoproductrelation\services\magento\model;


trait AttributeTrait {

    public function filterAttribute($attributes,$filter) : string {

        $return = '';

        foreach ($attributes as $attribute){

            if($attribute['attribute_code'] === $filter){

                $return = $attribute['value'];
                break;
            }
        }

        return $return;
    }

    /**
     * Check if product can be labeled as new by release date + X days
     *
     * @param $product
     * @return bool
     */
    public function isNewDate($releaseDate)
    {
        if($releaseDate && $releaseDate != ''){
            $showAsNewDays = 10;
            return time() < strtotime($releaseDate . "+$showAsNewDays days");
        }

        return false;
    }

    /**
     * Check if product is discounted by checking if price is different from finalprice
     *
     * @param $product
     * @return bool
     */
    public function isProductDiscounted($price,$discountPrice) : bool
    {
        $finalPrice = $price;
        $discountPrice = $discountPrice;

        // Prefer Brooklyn Solden Prices over normal discounted prices.
        if ($discountPrice > 0 && $discountPrice < $finalPrice) {
            $finalPrice = $discountPrice;
        }

        $regularPrice = $price;

        if ($finalPrice > 0
            && $finalPrice
            && $regularPrice
            && $finalPrice < $regularPrice
        ) {
            return $regularPrice != $finalPrice;
        }
        return false;
    }

    /**
     * Get discount percentage based on price and finalprice
     *
     * @param int $finalPrice
     * @param int $regularPrice
     * @return string
     */

    public function getDiscountPercent($finalPrice = 0, $regularPrice = 0)
    {
        if($finalPrice == 0 || $regularPrice == 0){
            return 0;
        }

        $percentage = number_format($finalPrice / $regularPrice * 100, 2);
        $discountPercentage = round(100 - $percentage);

        return $discountPercentage . '%';
    }

}
