<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:37
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\model\StoreView;
use glue\magentoproductrelation\services\magento\rest\products\ProductsClient;

class SearchRestFactory implements SearchFactory
{
    private $storeView;

    public function __construct(StoreView $storeView)
    {
        $this->storeView = $storeView;
    }

    public function createSearch(): Search
    {
        $restClient = new ProductsClient();
        $restClient->setStore($this->storeView);

        return new SearchRest($restClient);
    }

}
