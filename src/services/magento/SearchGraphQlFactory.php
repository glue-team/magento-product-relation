<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:37
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\graphql\Client as GraphQlClient;
use glue\magentoproductrelation\services\magento\model\StoreView;

class SearchGraphQlFactory implements SearchFactory
{
    private $storeView;

    public function __construct(StoreView $storeView)
    {
        $this->storeView = $storeView;
    }

    public function createSearch(): Search
    {
        $client = new GraphQlClient($this->storeView->code());

        return new SearchGraphQl($client);
    }

}
