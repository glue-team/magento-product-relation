<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 15:21
 */

namespace glue\magentoproductrelation\services\magento;

use League\Fractal\Serializer\ArraySerializer;

class ProductDataSerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }

    public function item($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }
}
