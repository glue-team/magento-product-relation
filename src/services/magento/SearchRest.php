<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:15
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\rest\products\ProductsByName;
use glue\magentoproductrelation\services\magento\rest\products\ProductsBySku;

class SearchRest implements Search
{

    private $client;

    public function __construct(MagentoClient $client)
    {
        $this->client = $client;
    }

    public function name(string $name, array $options = []): array
    {
        $search = new ProductsByName($this->client, $name, $options);

        return $search->toArray();
    }

    public function sku(array $skus, array $options = []): array
    {
        $search = new ProductsBySku($this->client, $skus, $options);

        return $search->toArray();
    }
}
