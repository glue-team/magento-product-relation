<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 13:35
 */

namespace glue\magentoproductrelation\services\magento\graphql;

interface QueryInterface
{

    public function __construct($searchField, $searchValue, $searchCondition);

    public function toString(): string;
}
