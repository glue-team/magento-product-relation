<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:09
 */

namespace glue\magentoproductrelation\services\magento\graphql;

use glue\magentoproductrelation\services\magento\MagentoClient;
use GuzzleHttp\Client as GuzzleClient;
use glue\magentoproductrelation\services\cache\CacheStack;
use glue\magentoproductrelation\MagentoProductRelation;

class Client extends MagentoClient
{
    private $storeView;

    public function __construct($storeView = null)
    {
        $this->storeView = $storeView;
        $this->setProductUri('/graphql');

        $client = new GuzzleClient([
            'handler'   =>  CacheStack::stack(),
            'headers' => $this->getHeaders(),
            'base_uri' => Magentoproductrelation::getInstance()->getSettings()->url,
        ]);

        parent::__construct($client);
    }

    public function getHeaders() : array {

        $headers = [
            'accept' => 'application/json'
        ];

        if(isset($this->storeView)){
            $headers['Store'] =  $this->storeView;
        }

        return $headers;
    }

}
