<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 15:59
 */

namespace glue\magentoproductrelation\services\magento\graphql;

use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Condition;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;


class ProductsBySku extends ProductsBase
{
    protected $skus;

    protected $options;

    public function __construct(MagentoClient $magentoClient, array $skus, array $options = [])
    {
        $this->skus = $skus;
        $this->options = $options;

        parent::__construct($magentoClient);
    }

    public function getSkus() : array {

        return $this->skus;
    }

    public function searchCondition(): string
    {
        return Condition::IN;
    }

    public function searchField(): string
    {
        return Field::SKU;
    }

    public function searchValue()
    {
        //["dog","cattt"]

        return '["' . implode('","',$this->skus) . '"]';
    }

    public function searchQuery() : string {

        $query = new Query();

        return $query->toString();
    }

    function currentPage(): int
    {
        return $this->options['current_page'] ?? 1;
    }
}
