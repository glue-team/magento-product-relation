<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 16:04
 */

namespace glue\magentoproductrelation\services\magento\graphql;

use GraphQL\RawObject;

class Query implements QueryInterface
{

    private $searchValue;

    private $searchCondition;

    private $searchField;

    public function __construct($searchField, $searchValue, $searchCondition)
    {
        $this->searchValue = $searchValue;
        $this->searchCondition = $searchCondition;
        $this->searchField = $searchField;
    }

    public function toString(): string
    {
        $gql = new \GraphQL\Query('products');

        $gql
            ->setArguments(['filter' => new RawObject('{ ' . $this->searchField . ': { ' . $this->searchCondition . ': ' . $this->searchValue . ' }}')])
            ->setSelectionSet([
                (new \GraphQL\Query('items'))
                    ->setSelectionSet([
                        'id',
                        'name',
                        'sku',
                        (new \GraphQL\Query('image'))
                            ->setSelectionSet([
                                'url',
                                'label',
                            ]),
                        'stock_status',
                        'canonical_url',
                        (new \GraphQL\Query('price'))
                            ->setSelectionSet([
                                (new \GraphQL\Query('regularPrice'))
                                    ->setSelectionSet([
                                        (new \GraphQL\Query('amount'))
                                            ->setSelectionSet([
                                                'value',
                                                'currency',
                                            ]),
                                    ]),
                            ]),
                    ]),
                'total_count',
                (new \GraphQL\Query('page_info'))
                    ->setSelectionSet([
                        'page_size',
                    ]),
            ]);

        return ((string) $gql);
    }
}
