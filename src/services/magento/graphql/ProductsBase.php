<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/11/19
 * Time: 16:56
 */

namespace glue\magentoproductrelation\services\magento\graphql;

use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\magento\config\Product;
use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\model\GraphQlProduct;
use glue\magentoproductrelation\services\magento\ProductCollection;
use glue\magentoproductrelation\services\magento\graphql\Client as GraphQlClient;
use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;

abstract class ProductsBase
{
    private $client;

    public function __construct(GraphQlClient $client)
    {
        $this->client = $client;
    }

    abstract function searchField() : string;
    abstract function searchValue();
    abstract function searchCondition() : string;
    abstract function searchQuery(): string;
    abstract function currentPage(): int;

    private function query() : string {

        $class = (new \glue\magentoproductrelation\services\magento\config\Query(InterfaceType::GRAPH_QL))->getClass();

        return (new $class(
            $this->searchField(),
            $this->searchValue(),
            $this->searchCondition(),
            $this->currentPage(),
        ))->toString();
    }

    public function toArray() : array
    {
        $magentoProducts = $this->client->endpointRequest(['query' => $this->query()]);

        if(!array_key_exists('data',$magentoProducts)){
            return [];
        }

        if(!$magentoProducts['data']['products']['items']){
            return [];
        }

        $skus = null;
        if(method_exists($this,'getSkus')){

            $skus = $this->getSkus();
        }

        $class = (new Product(InterfaceType::GRAPH_QL))->getClass();

        $productCollection = new ProductCollection($magentoProducts['data']['products']['items'],$class,InterfaceType::GRAPH_QL,$skus);

        return $productCollection->toArray();
    }
}
