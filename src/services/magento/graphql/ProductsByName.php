<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 15:59
 */

namespace glue\magentoproductrelation\services\magento\graphql;

use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Condition;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;


class ProductsByName extends ProductsBase
{
    private $name;

    protected $options;

    public function __construct(MagentoClient $magentoClient, string $name, array $options = [])
    {
        $this->name = $name;
        $this->options = $options;

        parent::__construct($magentoClient);
    }

    public function searchCondition(): string
    {
        return Condition::LIKE;
    }

    public function searchField(): string
    {
        return Field::NAME;
    }

    public function searchValue()
    {
        return '"%' . $this->name . '%"';
    }

    public function searchQuery() : string {

        $query = new Query();

        return $query->toString();
    }

    public function currentPage(): int
    {
        return $this->options['current_page'] ?? 1;
    }
}
