<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 09:44
 */

namespace glue\magentoproductrelation\services\magento;


use glue\magentoproductrelation\services\magento\model\StoreView;

class SearchSelector
{
    private $serviceType;
    private $storeView;

    public function __construct($serviceType,StoreView $storeView)
    {
        $this->serviceType = $serviceType;
        $this->storeView = $storeView;
    }

    public function factory() : SearchFactory {

        switch ($this->serviceType){

            case SearchServiceType::REST :
                $factory = new SearchRestFactory($this->storeView);
                break;
            case SearchServiceType::GRAPH_QL :
                $factory = new SearchGraphQlFactory($this->storeView);
                break;
            default :
                $factory = new SearchRestFactory($this->storeView);
        }

        return $factory;
    }

}
