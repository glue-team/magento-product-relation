<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 27/01/20
 * Time: 09:14
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\model\Product;

class ProductIterator implements \Iterator
{
    /** @var int */
    private $position = 0;

    private $productCollection;

    public function __construct(ProductCollection $productCollection)
    {
        $this->productCollection = $productCollection;
    }

    public function current() : Product
    {
        return $this->productCollection->getProduct($this->position);
    }

    public function next()
    {
        $this->position++;
    }

    public function key() : int
    {
        return $this->position;
    }

    public function valid() : bool
    {
        return !is_null($this->productCollection->getProduct($this->position));
    }

    public function rewind()
    {
        $this->position = 0;
    }

}
