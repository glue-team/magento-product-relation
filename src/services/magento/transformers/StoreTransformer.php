<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:44
 */

namespace glue\magentoproductrelation\services\magento\transformers;

use League\Fractal\TransformerAbstract;

use glue\magentoproductrelation\services\magento\model\StoreView;

class StoreTransformer extends TransformerAbstract
{

    public function transform(StoreView $store)
    {
        return [

            'id'        =>  $store->id(),
            'code'      =>  $store->code(),
            'name'      =>  $store->name(),
            'website_id'        =>  $store->websiteId(),
            'store_group_id'    =>  $store->storeGroupId(),
            'is_active'         =>  $store->isActive(),

            'label' =>  $store->name() . ' (' .$store->code(). ')',
            'value' =>  $store->id(),
        ];
    }

}
