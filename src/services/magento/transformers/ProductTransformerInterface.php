<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 11:27
 */

namespace glue\magentoproductrelation\services\magento\transformers;

use glue\magentoproductrelation\services\magento\model\Product;


interface ProductTransformerInterface {

    public function transform(Product $product) : array ;

}
