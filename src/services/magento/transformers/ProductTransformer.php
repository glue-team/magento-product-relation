<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 10:44
 */

namespace glue\magentoproductrelation\services\magento\transformers;

use League\Fractal\TransformerAbstract;

use glue\magentoproductrelation\services\magento\model\Product;

class ProductTransformer extends TransformerAbstract implements ProductTransformerInterface
{

    public function transform(Product $product) : array
    {
        return [

            'name'          =>  $product->name(),
            'sku'           =>  $product->sku(),
            'price'         =>  $product->price(),
            'id'    =>  $product->id(),
            'image' =>  $product->imageUrl(),
            'url'   =>  $product->url(),

            'isNew' =>  $product->isNew(),
            'isDiscounted'  =>  $product->isDiscounted(),
            'discountPrice'         =>  $product->discountPrice(),
            'discountPercentage'    =>  $product->discountPercentage()
        ];
    }

}
