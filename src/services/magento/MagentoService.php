<?php

namespace glue\magentoproductrelation\services\magento;

use Craft;
use glue\magentoproductrelation\Exceptions\MagentoProductRelationException;
use glue\magentoproductrelation\Exceptions\MagentoServiceNotInitializedException;
use glue\magentoproductrelation\Exceptions\StoreViewNotFoundException;
use glue\magentoproductrelation\Exceptions\StoreViewNotSpecifiedException;
use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\magento\model\StoreView as DataStoreView;
use glue\magentoproductrelation\services\magento\rest\stores\StoreView;

class MagentoService
{

    /**
     * @throws StoreViewNotFoundException
     */
    public static function init()
    {
        $self = new static;
        $settings = MagentoProductRelation::getInstance()->getSettings();

        try {
            $storeView = $self->currentStoreView();
            $selector = new SearchSelector($settings['service_type'], $storeView);

            Craft::$container->set(
                SearchFactory::class, $selector->factory()
            );
        } catch(MagentoProductRelationException $e) {
            throw new MagentoServiceNotInitializedException($e->getMessage());
        }
    }

    /**
     * @throws StoreViewNotFoundException
     *
     * @return DataStoreView
     */
    protected function currentStoreView(): DataStoreView
    {
        $craftSetting = MagentoProductRelation::getInstance()->getSettings()['craft_site_magento_store_view'];

        //bug in craft...?
        //always returns the current site
        $currentSite = Craft::$app->getSites()->getCurrentSite();

        if(isset(Craft::$app->request)
            && Craft::$app->request instanceof craft\web\Request
            && Craft::$app->request->get(MagentoProductRelation::getInstance()->getSettings()->getSiteParam())) {
            $currentSite = Craft::$app->getSites()->getSiteByHandle(Craft::$app->request->get(MagentoProductRelation::getInstance()->getSettings()->getSiteParam()));
        }

        if(! array_key_exists($currentSite->id, $craftSetting)) {
            throw new StoreViewNotSpecifiedException("StoreView for site '{$currentSite->name}' is not set.");
        }

        $storeViewId = $craftSetting[$currentSite->id];

        $storeViewCollection = new StoreView();
        $storeView = $storeViewCollection->find($storeViewId);

        if($storeViewCollection->isEmpty() || is_null($storeView)) {
            throw new StoreViewNotFoundException("StoreView with id '{$storeViewId}' not found.");
        }

        return $storeView;
    }
}
