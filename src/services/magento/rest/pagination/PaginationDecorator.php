<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 13:23
 */

namespace glue\magentoproductrelation\services\magento\rest\pagination;

class PaginationDecorator
{

    protected $searchCriteria;

    public function __construct(array $searchCriteria, $total_count)
    {
        $this->searchCriteria = $searchCriteria;
        $this->searchCriteria['total_count'] = $total_count;
    }

    public function toArray(): array
    {
        return [
            'previous_page' => $this->getPreviousPage(),
            'current_page'  => $this->current_page,
            'next_page'     => $this->getNextPage(),
            'page_size'     => $this->page_size,
            'first_item'    => $this->getFirstItem(),
            'last_item'     => $this->getLastItem(),
            'total_count'   => $this->total_count,
        ];
    }

    protected function getPreviousPage(): ?int
    {
        if($this->current_page <= 1) {
            return null;
        }

        return $this->current_page - 1;
    }

    protected function getNextPage(): ?int
    {
        if($this->current_page === $this->getTotalPages()) {
            return null;
        }

        return $this->current_page + 1;
    }

    protected function getTotalPages(): ?int
    {
        return ceil($this->total_items / $this->page_size);
    }

    protected function getFirstItem(): ?int
    {
        return ($this->current_page * $this->page_size) - ($this->page_size - 1);
    }

    protected function getLastItem(): ?int
    {
        if($this->getNextPage()) {
            return $this->current_page * $this->page_size;
        }

        return $this->total_count;
    }

    public function __get($name): ?int
    {
        return isset($this->searchCriteria[$name]) ? $this->searchCriteria[$name] : null;
    }
}
