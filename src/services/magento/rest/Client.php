<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/01/20
 * Time: 13:51
 */

namespace glue\magentoproductrelation\services\magento\rest;

use glue\magentoproductrelation\services\magento\model\StoreView;
use GuzzleHttp\Client as HttpClient;

use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\cache\CacheStack;

class Client extends MagentoClient
{
    const REST_PREFIX = 'rest';

    const REST_VERSION = 'V1';

    /**
     * @var StoreView
     */
    private $store;

    /**
     * @var string
     */
    private $uri;

    public function __construct($uri)
    {
        $this->uri = $uri;
        $client = new HttpClient([
            'handler'   =>  CacheStack::stack(),
            'headers' => [
                'accept' => 'application/json',
                'Authorization' => 'Bearer ' .  Magentoproductrelation::getInstance()->getSettings()->authToken,
            ],
            'base_uri' => Magentoproductrelation::getInstance()->getSettings()->url,
        ]);

        $this->setProductUri('/' . self::REST_PREFIX . '/' . self::REST_VERSION . '/'. $uri);

        parent::__construct($client);
    }

    public function setStore(StoreView $store){
        $this->store = $store;
        $this->setProductUri('/' . self::REST_PREFIX . '/' . $this->store->code() . '/' . self::REST_VERSION . '/'. $this->uri);
    }

    public function getStore(){
        return $this->store;
    }


}
