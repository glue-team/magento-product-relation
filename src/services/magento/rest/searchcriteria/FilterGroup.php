<?php

namespace glue\magentoproductrelation\services\magento\rest\searchcriteria;

use yii\debug\components\search\Filter;

class FilterGroup
{

    private $filters;

    public function __construct(array $filters)
    {
        $this->setFilters($filters);
    }

    public function addFilter(Filter $filter): array
    {
        $this->filters[] = $filter;

        return $this->filters;
    }

    public function setFilters(array $filters): array
    {
        return $this->filters = $filters;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function toArray() {
        $filterGroup = [];

        foreach($this->filters as $filter) {
            $filterGroup['filters'][] = $filter->toArray();
        }

        return $filterGroup;
    }
}
