<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:05
 */

namespace glue\magentoproductrelation\services\magento\rest\searchcriteria;

use glue\magentoproductrelation\services\magento\settings\searchcriteria\Builder;

class RestBuilder extends Builder
{

    protected $filterGroups;

    protected $currentPage;

    protected $pageSize;

    public function addFilterGroup(array $filters): self
    {
        $this->filterGroups[] = new FilterGroup($filters);

        return $this;
    }

    public function setCurrentPage(int $currentPage): self
    {
        $this->currentPage = $currentPage;

        return $this;
    }

    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    //fields=items[id,sku,name,qty]

    public function toArray(): array
    {
        //https://devdocs.magento.com/guides/v2.3/rest/performing-searches.html

        $filters = [];

        foreach($this->filterGroups as $filterGroup) {
            $filters['searchCriteria']['filter_groups'][] = $filterGroup->toArray();
        }

        $filters['searchCriteria']['currentPage'] = $this->currentPage;
        $filters['searchCriteria']['pageSize'] = $this->pageSize;

        if(isset($this->fields) && is_array($this->fields)) {

            $filters['fields'] = 'items[' . implode(',', $this->fields) . '],search_criteria,total_count';
        }

        return $filters;
    }
}
