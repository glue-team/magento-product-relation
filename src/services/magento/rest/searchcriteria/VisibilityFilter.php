<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 10:02
 */

namespace glue\magentoproductrelation\services\magento\rest\searchcriteria;

use glue\magentoproductrelation\services\magento\settings\searchcriteria\Condition;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Filter;

class VisibilityFilter extends Filter
{

    CONST FIELD = 'visibility';

    public function __construct($value, $condition = Condition::EQUALS)
    {
        if(is_array($value)) {
            $condition = Condition::IN;
            $value = implode(',', $value);
        }

        parent::__construct(self::FIELD, $value, $condition);
    }
}
