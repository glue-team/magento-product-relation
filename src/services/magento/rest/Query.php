<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 08:58
 */

namespace glue\magentoproductrelation\services\magento\rest;

use glue\magentoproductrelation\services\magento\rest\searchcriteria\RestBuilder;
use glue\magentoproductrelation\services\magento\rest\searchcriteria\VisibilityFilter;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Filter;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Visibility;

class Query implements QueryInterface
{

    protected $searchField;

    protected $searchValue;

    protected $searchCondition;

    protected $currentPage;

    protected $pageSize;

    public function __construct($searchField, $searchValue, $searchCondition, $currentPage, $pageSize)
    {
        $this->searchValue = $searchValue;
        $this->searchCondition = $searchCondition;
        $this->searchField = $searchField;
        $this->currentPage = $currentPage;
        $this->pageSize = $pageSize;
    }

    public function toArray(): array
    {
        $builder = new RestBuilder();

        $builder
            ->addFilterGroup([
                new Filter(
                    $this->searchField,
                    $this->searchValue,
                    $this->searchCondition
                ),
            ])
            ->addFilterGroup([
                new VisibilityFilter(Visibility::VISIBILITY_BOTH),
            ])
            ->addField(Field::ID)
            ->addField(Field::SKU)
            ->addField(Field::NAME)
            ->addField(Field::PRICE)
            ->addField(Field::TYPE_ID)
            ->addField(Field::CUSTOM_ATTRIBUTES)
            ->setCurrentPage($this->currentPage)
            ->setPageSize($this->pageSize);

        return $builder->toArray();
    }
}
