<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 11:59
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;

class ProductByStore
{
    private $product;
    private $storeId;

    public function __construct(array $product,int $storeId)
    {
        $this->product = $product;
        $this->storeId = $storeId;
    }

    public function get(){

        $sku = $this->product[Field::SKU];
        $client = new ProductByStoreClient($sku);

        return $client->endpointRequest(['storeId' => $this->storeId]);
    }

}
