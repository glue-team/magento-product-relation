<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 11:59
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\settings\AttributeCode;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;
use glue\magentoproductrelation\services\magento\model\AttributeTrait;

class ConfigurableProduct
{
    use AttributeTrait;

    private $product;
    private $children;

    public function __construct(array $product)
    {
        $this->product = $product;
        $this->children();
    }

    private function children(){

        $sku = $this->product[Field::SKU];
        $client = new ConfigurableProductClient($sku);
        $this->children = $client->endpointRequest();
    }

    public function price(){

        $price = isset($this->product[Field::PRICE]) ? $this->product[Field::PRICE] : '';
        $prices = [];

        foreach ($this->children as $child){

            if(isset($child[Field::PRICE]) && (int)$child[Field::PRICE] > 0){

                $prices[] = $child[Field::PRICE];
            }
        }

        //if there is a value different from 0

        if(!empty($prices)){
            $price = min($prices);
        }

        return $price;
    }

    public function discountPrice(){

        $price = 0;

        foreach ($this->children as $child){

            $price = $this->filterAttribute($this->product['custom_attributes'],AttributeCode::DISCOUNT_PRICE);
            break;
        }

        return $price;
    }

}
