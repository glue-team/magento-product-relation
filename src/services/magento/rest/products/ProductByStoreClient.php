<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 12:04
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\rest\Client;

class ProductByStoreClient extends Client
{
    protected $storeId;

    public function __construct($sku)
    {
        $uri = strtr('products/:sku',[':sku' => $sku]);

        parent::__construct($uri);
    }
}
