<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/11/19
 * Time: 16:56
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Condition;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;

class ProductsByName extends ProductsBase
{

    private $name;

    protected $options;

    public function __construct(MagentoClient $magentoClient, string $name, array $options = [])
    {
        $this->name = $name;
        $this->options = $options;

        parent::__construct($magentoClient);
    }

    public function searchCondition(): string
    {
        return Condition::LIKE;
    }

    public function searchField(): string
    {
        return Field::NAME;
    }

    public function searchValue()
    {
        return '%' . rawurlencode(htmlentities($this->name)) . '%';
    }

    public function currentPage(): int
    {
        return $this->options['current_page'] ?? 1;
    }

    public function pageSize(): int
    {
        return 20;
    }
}
