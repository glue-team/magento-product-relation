<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/11/19
 * Time: 16:56
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\magento\config\Product;
use glue\magentoproductrelation\services\magento\model\RestProduct;
use glue\magentoproductrelation\services\magento\ProductCollection;
use glue\magentoproductrelation\services\magento\rest\pagination\PaginationDecorator;
use glue\magentoproductrelation\services\magento\rest\Query;
use glue\magentoproductrelation\services\magento\rest\Client as RestClient;
use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;

abstract class ProductsBase
{
    private $client;

    public function __construct(RestClient $client)
    {
        $this->client = $client;
    }

    abstract function searchField() : string;
    abstract function searchValue();
    abstract function searchCondition() : string;
    abstract function currentPage() : int;
    abstract function pageSize() : int;

    public function buildCriteria() : array {

        $class = (new \glue\magentoproductrelation\services\magento\config\Query(InterfaceType::REST))->getClass();

        return (new $class(
            $this->searchField(),
            $this->searchValue(),
            $this->searchCondition(),
            $this->currentPage(),
            $this->pageSize()
        ))->toArray();

    }

    public function toArray() : array
    {
        $magentoProducts = $this->client->endpointRequest($this->buildCriteria());

        if(!isset($magentoProducts) || !is_array($magentoProducts) || !array_key_exists('items',$magentoProducts)
        || is_null($magentoProducts['items'])
        ) {
            return [
                'products' => [],
                'pagination' => [],
            ];
        }

        $skus = null;
        if(method_exists($this,'getSkus')){

            $skus = $this->getSkus();
        }

        $class = (new Product(InterfaceType::REST))->getClass();

        $productsDecorated = new ProductDecorator($magentoProducts['items'],$this->client->getStore()->id());
        $productsCollection = new ProductCollection($productsDecorated->toArray(), $class,InterfaceType::REST,$skus);

        $paginationDecorated = new PaginationDecorator($magentoProducts['search_criteria'], $magentoProducts['total_count']);

        return [
            'products' => $productsCollection->toArray(),
            'pagination' => $paginationDecorated->toArray(),
        ];
    }
}
