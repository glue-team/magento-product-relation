<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 12:04
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\rest\Client;

class ProductsClient extends Client
{
    public function __construct()
    {
        parent::__construct('products/');
    }
}
