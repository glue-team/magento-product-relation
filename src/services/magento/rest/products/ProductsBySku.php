<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/11/19
 * Time: 16:56
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Condition;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;

class ProductsBySku extends ProductsBase
{
    private $skus;

    protected $options;

    public function __construct(MagentoClient $magentoClient, array $skus, array $options = [])
    {
        $this->skus = $skus;
        $this->options = $options;

        parent::__construct($magentoClient);
    }

    public function getSkus() {

        return $this->skus;
    }

    public function searchCondition(): string
    {
        return Condition::IN;
    }

    public function searchValue()
    {
        return implode(',', $this->skus);
    }

    public function searchField(): string
    {
        return Field::SKU;
    }

    public function currentPage(): int
    {
        return $this->options['current_page'] ?? 1;
    }

    public function pageSize(): int
    {
        return count($this->skus);
    }
}
