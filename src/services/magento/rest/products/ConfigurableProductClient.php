<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 12:04
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\rest\Client;

class ConfigurableProductClient extends Client
{
    public function __construct($sku)
    {
        $uri = strtr('configurable-products/:sku/children',[':sku' => $sku]);

        parent::__construct($uri);
    }
}
