<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 13:23
 */

namespace glue\magentoproductrelation\services\magento\rest\products;

use glue\magentoproductrelation\services\magento\settings\AttributeCode;
use glue\magentoproductrelation\services\magento\settings\searchcriteria\Field;
use glue\magentoproductrelation\services\magento\settings\ProductType;

class ProductDecorator
{
    private $products;
    private $list = [];
    private $storeId;

    public function __construct(array $products,$storeId)
    {
        $this->products = $products;
        $this->storeId = $storeId;
    }

    private function product(array $item) : array {

        //get the product detail
        //override the base data from the product
        //with the storeView data

        //issue REST api gives the product data off all storeViews
        //instead off the filtered products off the given storeView
        //https://github.com/magento/magento2/issues/15461#issuecomment-503957573

        $storeProduct = new ProductByStore($item,$this->storeId);
        $item = array_merge($item,$storeProduct->get());

        if ($item[Field::TYPE_ID] === ProductType::CONFIGURABLE) {

            $configProduct = new ConfigurableProduct($item);
            $item[Field::PRICE] = $configProduct->price();
            $item[AttributeCode::DISCOUNT_PRICE] = $configProduct->discountPrice();
        }

        return $item;
    }

    public function toArray() : array {

        foreach ($this->products as $index => $item) {

            $this->list[] = $this->product($item);
        }

        return $this->list;
    }

}
