<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/01/20
 * Time: 14:16
 */

namespace glue\magentoproductrelation\services\magento\rest\stores;


use glue\magentoproductrelation\services\magento\StoreViewCollection;
use glue\magentoproductrelation\services\magento\model\StoreView as ModelStoreView;

class StoreView
{
    private $client;

    public function __construct()
    {
        $this->client = new StoreViewClient();
    }

    private function collection() : StoreViewCollection {

        return new StoreViewCollection($this->client->endpointRequest());
    }

    public function find(int $id) : ?ModelStoreView {

        return $this->collection()->getStoreView($id);
    }

    public function isEmpty() : bool {
        return $this->collection()->isEmpty();
    }

    public function toArray() : array {

        return $this->collection()->toArray();
    }

    public function toJson() : string {

        return json_encode($this->toArray());
    }
}
