<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 29/11/19
 * Time: 12:04
 */

namespace glue\magentoproductrelation\services\magento\rest\stores;

use glue\magentoproductrelation\Exceptions\UnauthorizedException;
use glue\magentoproductrelation\services\magento\rest\Client as RestClient;

class StoreViewClient extends RestClient
{

    public function __construct()
    {
        parent::__construct('store/storeViews');
    }

    public function toArray(): array
    {

        return $this->endpointRequest();
    }

    public function toJson(): string
    {

        return json_encode($this->toArray());
    }
}
