<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 11:05
 */

namespace glue\magentoproductrelation\services\magento\rest;

interface QueryInterface
{

    public function __construct($searchField, $searchValue, $searchCondition, $currentPage, $pageSize);

    public function toArray(): array;
}
