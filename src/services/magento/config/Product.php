<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:25
 */

namespace glue\magentoproductrelation\services\magento\config;


use glue\magentoproductrelation\services\magento\model\GraphQlProduct;
use glue\magentoproductrelation\services\magento\model\RestProduct;
use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;
use glue\magentoproductrelation\services\magento\transformers\ProductTransformer;

class Product extends BaseConfig
{
    protected $defaults = [
      InterfaceType::REST =>    RestProduct::class,
      InterfaceType::GRAPH_QL   =>  GraphQlProduct::class,
    ];

    public function __construct($serviceType)
    {
        parent::__construct($serviceType);
        $this->setDefaults($this->defaults);
    }

    public function getClass(){

        return $this->lookup($this->serviceType,InterfaceTypeVariant::PRODUCT);
    }

}
