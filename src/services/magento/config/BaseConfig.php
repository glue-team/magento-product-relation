<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:27
 */

namespace glue\magentoproductrelation\services\magento\config;

use glue\magentoproductrelation\MagentoProductRelation;

abstract class BaseConfig
{
    protected $serviceType;
    protected $defaults;

    public function __construct($serviceType)
    {
        $this->serviceType = $serviceType;
    }

    public function setDefaults(array $defaults)
    {
        return $this->defaults = $defaults;
    }

    public function default($serviceType){

        return $this->defaults[$serviceType];
    }

    protected function lookup($serviceType,$key){

        $queryClass = MagentoProductRelation::getInstance()->getOverrideByTypeAndKey($serviceType,$key);
        $class = $this->default($serviceType);

        if(isset($queryClass)){
            $class = $queryClass;
        }

        return $class;

    }

}
