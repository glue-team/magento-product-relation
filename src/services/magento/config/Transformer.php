<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:25
 */

namespace glue\magentoproductrelation\services\magento\config;

use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;
use glue\magentoproductrelation\services\magento\transformers\ProductTransformer;

class Transformer extends BaseConfig
{
    protected $defaults = [
      InterfaceType::REST       =>      ProductTransformer::class,
      InterfaceType::GRAPH_QL   =>      ProductTransformer::class,
    ];

    public function __construct($serviceType)
    {
        parent::__construct($serviceType);
        $this->setDefaults($this->defaults);
    }

    public function getClass(){

        return $this->lookup($this->serviceType,InterfaceTypeVariant::TRANSFORMER);
    }

}
