<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 17/02/20
 * Time: 14:25
 */

namespace glue\magentoproductrelation\services\magento\config;


use glue\magentoproductrelation\services\magento\settings\InterfaceType;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;

class Query extends BaseConfig
{
    protected $defaults = [
      InterfaceType::REST =>    \glue\magentoproductrelation\services\magento\rest\Query::class,
      InterfaceType::GRAPH_QL   =>  \glue\magentoproductrelation\services\magento\graphql\Query::class,
    ];

    public function __construct($serviceType)
    {
        parent::__construct($serviceType);
        $this->setDefaults($this->defaults);
    }

    public function getClass(){

        return $this->lookup($this->serviceType,InterfaceTypeVariant::QUERY);
    }

}
