<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 27/01/20
 * Time: 10:33
 */

namespace glue\magentoproductrelation\services\magento;


use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\magento\config\Transformer;
use glue\magentoproductrelation\services\magento\model\Product;
use glue\magentoproductrelation\services\magento\settings\InterfaceTypeVariant;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

use glue\magentoproductrelation\services\magento\transformers\ProductTransformer;

class ProductCollection
{
    private $products = [];
    private $productClass;
    protected $serviceType;
    private $skus = [];

    public function __construct(array $products,string $productClass,$serviceType,$skus = null)
    {
        $this->productClass = $productClass;
        $this->serviceType = $serviceType;
        $this->skus = $skus;

        $this->convert($products);
        $this->orderBySkus();
    }

    public function convert($products){

        foreach ($products as $product) {

            $this->addProduct(new $this->productClass($product));
        }
    }

    private function orderBySkus(){

        if(is_null($this->skus)){
            return;
        }

        $this->products = $this->orderArray($this->products,$this->skus);
    }

    protected function orderArray($arrayToOrder, $keys) {
        $ordered = array();
        foreach ($keys as $key) {
            if (isset($arrayToOrder[$key])) {
                $ordered[$key] = $arrayToOrder[$key];
            }
        }
        return $ordered;
    }

    public function getIterator() : ProductIterator
    {
        return new ProductIterator($this);
    }

    public function getProduct($position)
    {
        if (isset($this->products[$position])) {
            return $this->products[$position];
        }

        return null;
    }

    public function addProduct(Product $product)
    {
        $this->products[$product->getSku()] = $product;
    }

    public function toCollection() : Collection {

        $class = (new Transformer($this->serviceType))->getClass();

        return new Collection($this->products, new $class());
    }

    public function toArray() : array {

        $manager = new Manager();
        $serializer = new ProductDataSerializer();
        $manager->setSerializer($serializer);

        return $manager->createData($this->toCollection())->toArray();
    }

}
