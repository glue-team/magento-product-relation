<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:40
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\graphql\ProductsByName;
use glue\magentoproductrelation\services\magento\graphql\ProductsBySku;

class SearchGraphQl implements Search {

    private $client;

    public function __construct(MagentoClient $client)
    {
        $this->client = $client;
    }

    public function name(string $name, $options = []): array
    {
        $search = new ProductsByName($this->client,$name);
        return $search->toArray();
    }

    public function sku(array $skus, $options = []): array
    {
        $search = new ProductsBySku($this->client,$skus);
        return $search->toArray();
    }
}
