<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 23/01/20
 * Time: 14:18
 */

namespace glue\magentoproductrelation\services\magento;

interface  Search {

    public function __construct(MagentoClient $client);

    public function name(string $name, array $options = []) : array;

    public function sku(array $skus, array $options = []) : array;
}
