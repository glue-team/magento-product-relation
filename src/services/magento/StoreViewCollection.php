<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 27/01/20
 * Time: 10:33
 */

namespace glue\magentoproductrelation\services\magento;

use glue\magentoproductrelation\services\magento\model\StoreView;
use glue\magentoproductrelation\services\magento\transformers\StoreTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

class StoreViewCollection
{
    private $storeViews = [];

    public function __construct(array $storeViews)
    {
        $this->convert($storeViews);
    }

    public function convert($storeViews){

        foreach ($storeViews as $storeView) {

            $this->addStoreView(new StoreView($storeView));
        }
    }

    public function getStoreView($position) : ?StoreView
    {
        if (isset($this->storeViews[$position])) {
            return $this->storeViews[$position];
        }

        return null;
    }

    public function isEmpty(): bool
    {
        return empty($this->storeViews);
    }

    public function addStoreView(StoreView $storeView)
    {
        $this->storeViews[$storeView->id()] = $storeView;
    }

    public function toCollection() : Collection {

        return new Collection($this->storeViews, new StoreTransformer());
    }

    public function toArray() : array {

        $manager = new Manager();
        $serializer = new ProductDataSerializer();
        $manager->setSerializer($serializer);

        return $manager->createData($this->toCollection())->toArray();
    }

}
