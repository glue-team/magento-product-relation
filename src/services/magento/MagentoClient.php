<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 28/11/19
 * Time: 14:45
 */

namespace glue\magentoproductrelation\services\magento;

use Craft;
use glue\magentoproductrelation\Exceptions\ConnectionException;
use glue\magentoproductrelation\Exceptions\RequestException;
use glue\magentoproductrelation\Exceptions\UnauthorizedException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;

class MagentoClient
{
    private $productUri;
    private $client;

    public function __construct(Client $client)
    {
        if(!$this->productUri){
            throw new \Exception('no uri set');
        }

        $this->client = $client;
    }

    public function setProductUri($productUri){

        $this->productUri = $productUri;
    }

    public function endpointRequest(array $parameters = []): array
    {
        try {
            $response = $this->client->get($this->productUri, [
                'query'  => $parameters,
                'verify' => false,
            ]);

            $data = json_decode($response->getBody()->getContents(), true);

            if($data === null) {
                return [];
            }

            return $data;
        } catch(ConnectException $e) {
            throw new ConnectionException;
        } catch(GuzzleException $e) {
            Craft::error($e->getTraceAsString(), __CLASS__);

            switch($e->getCode()) {
                case 401:
                    throw new UnauthorizedException;
                    break;
                default:
                    throw new RequestException($e->getCode(), $e->getMessage());
            }
        }

        return [];
    }
}
