<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 2/12/19
 * Time: 11:06
 */

namespace glue\magentoproductrelation\services\cache;

use GuzzleHttp\HandlerStack;

use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;

class CacheStack
{

    public static function stack() : HandlerStack
    {
        // Create default HandlerStack
        $stack = HandlerStack::create();
        $duration = self::duration();

        // Add this middleware to the top with `push`
        $stack->push(new CacheMiddleware(
            new GreedyCacheStrategy(
                new CraftCache()
                ,$duration
            )

        ), 'cache');

        return $stack;
    }

    public static function duration() : int {

        $config = \Craft::$app->config->getConfigFromFile('magento-product-relation');

        if(isset($config['cacheDuration'])){

            return (int)$config['cacheDuration'];
        }

        return 60 * 60;
    }


}
