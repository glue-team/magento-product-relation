<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 24/01/20
 * Time: 13:31
 */

namespace glue\magentoproductrelation\services\cache;

use Kevinrob\GuzzleCache\CacheEntry;
use Kevinrob\GuzzleCache\Storage\CacheStorageInterface;

class CraftCache implements CacheStorageInterface
{
    private $cache;

    public function __construct()
    {
        $this->cache = \Craft::$app->getCache();
    }

    public function fetch($key)
    {
        if ($this->cache->exists($key)) {
            // The file exist, read it!
            $data = @unserialize(
                $this->cache->get($key)
            );

            if ($data instanceof CacheEntry) {
                return $data;
            }
        }

        return;
    }

    public function save($key, CacheEntry $data)
    {
        return $this->cache->set($key, serialize($data));

    }

    public function delete($key)
    {
        return $this->cache->delete($key);

    }

}
