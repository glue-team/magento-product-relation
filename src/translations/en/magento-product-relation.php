<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

/**
 * MagentoProductRelation en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('magento-product-relation', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Glue
 * @package   MagentoProductRelation
 * @since     1.0.2
 */
return [
    'MagentoProductRelation plugin loaded' => 'MagentoProductRelation plugin loaded',
];
