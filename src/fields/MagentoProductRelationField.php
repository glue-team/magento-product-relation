<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

namespace glue\magentoproductrelation\fields;

use Exception;
use glue\magentoproductrelation\Exceptions\MagentoProductRelationException;
use glue\magentoproductrelation\Exceptions\RequestException;
use glue\magentoproductrelation\MagentoProductRelation;

use glue\magentoproductrelation\assetbundles\magentoproductrelationfieldfield\MagentoProductRelationFieldFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use glue\magentoproductrelation\services\magento\MagentoService;
use glue\magentoproductrelation\services\magento\model\StoreView as DataStoreView;
use glue\magentoproductrelation\services\magento\rest\stores\StoreView;
use glue\magentoproductrelation\services\magento\SearchFactory;
use glue\magentoproductrelation\services\magento\SearchSelector;
use nystudio107\seomatic\models\jsonld\Car;
use yii\db\Schema;
use craft\helpers\Json;
use glue\magentoproductrelation\services\magento\SearchService;
use yii\di\NotInstantiableException;

/**
 * MagentoProductRelationField Field
 *
 * Whenever someone creates a new field in Craft, they must specify what
 * type of field it is. The system comes with a handful of field types baked in,
 * and we’ve made it extremely easy for plugins to add new ones.
 *
 * https://craftcms.com/docs/plugins/field-types
 *
 * @author    Glue
 * @package   MagentoProductRelation
 * @since     1.0.2
 */
class MagentoProductRelationField extends Field
{
    // Public Properties
    // =========================================================================

    /**
     * Some attribute
     *
     * @var string
     */
    public $someAttribute = 'Some Default';

    // Static Methods
    // =========================================================================

    /**
     * Returns the display name of this class.
     *
     * @return string The display name of this class.
     */
    public static function displayName(): string
    {
        return Craft::t('magento-product-relation', 'MagentoProductRelationField');
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }

    /**
     * Returns the column type that this field should get within the content table.
     *
     * This method will only be called if [[hasContentColumn()]] returns true.
     *
     * @return string The column type. [[\yii\db\QueryBuilder::getColumnType()]] will be called
     * to convert the give column type to the physical one. For example, `string` will be converted
     * as `varchar(255)` and `string(100)` becomes `varchar(100)`. `not null` will automatically be
     * appended as well.
     * @see \yii\db\QueryBuilder::getColumnType()
     */
    public function getContentColumnType(): string
    {
        return Schema::TYPE_STRING;
    }

    /**
     * Normalizes the field’s value for use.
     *
     * This method is called when the field’s value is first accessed from the element. For example, the first time
     * `entry.myFieldHandle` is called from a template, or right before [[getInputHtml()]] is called. Whatever
     * this method returns is what `entry.myFieldHandle` will likewise return, and what [[getInputHtml()]]’s and
     * [[serializeValue()]]’s $value arguments will be set to.
     *
     * @param mixed                 $value   The raw field value
     * @param ElementInterface|null $element The element the field is associated with, if there is one
     *
     * @return mixed The prepared field value
     */
    public function normalizeValue($value, ElementInterface $element = null)
    {
        if(is_array($value)){
            $value = implode(',',$value);
        }

        return $value;
    }

    /**
     * Modifies an element query.
     *
     * This method will be called whenever elements are being searched for that may have this field assigned to them.
     *
     * If the method returns `false`, the query will be stopped before it ever gets a chance to execute.
     *
     * @param ElementQueryInterface $query The element query
     * @param mixed                 $value The value that was set on this field’s corresponding [[ElementCriteriaModel]] param,
     *                                     if any.
     *
     * @return null|false `false` in the event that the method is sure that no elements are going to be found.
     */
    public function serializeValue($value, ElementInterface $element = null)
    {
        return parent::serializeValue($value, $element);
    }

    public function getSettingsHtml()
    {
        // Render the settings template
        return Craft::$app->getView()->renderTemplate(
        // 'magento-product-relation/_components/fields/MagentoProductRelationField_settings',
            'magento-product-relation/_components/fields/_settings',
            [
                'field' => $this,
            ]
        );
    }

    public function getInputHtml($value, ElementInterface $element = null): string
    {
        try {
            // Initialise the service
            MagentoService::init();

            // This is for the real Data
            $skus = explode(',', $value);

            $search = Craft::$container->get(SearchService::class);
            $content = $search->sku($skus);
        } catch(MagentoProductRelationException $e) {
            Craft::$app->session->setError("Magento Plugin: {$e->getMessage()}");
        } catch(NotInstantiableException $e ) {
            Craft::$app->session->setError('Magento Plugin: There is a problem initialising the plugin.');
        } catch(Exception $e) {
            Craft::$app->session->setError('Magento Plugin: Undefined issue.');
        }

        // Register our asset bundle
        Craft::$app->getView()->registerAssetBundle(MagentoProductRelationFieldFieldAsset::class);

        // Get our id and namespace
        $id = Craft::$app->getView()->formatInputId($this->handle);
        $namespacedId = Craft::$app->getView()->namespaceInputId($id);

        $inputNamespaced = Craft::$app->getView()->namespaceInputName($id);

        $site = ($element ? $element->getSite() : Craft::$app->getSites()->getCurrentSite());

        // Variables to pass down to our field JavaScript to let it namespace properly
        $jsonVars = [
            'id' => $id,
            'name' => $this->handle,
            'elementSiteId' => $site->id,
            'namespace' => $namespacedId,
            'namespacedId' => $namespacedId,
            'inputNamespaced'   =>  $inputNamespaced,
            'prefix' => Craft::$app->getView()->namespaceInputId(''),
        ];

        $jsonVars = Json::encode($jsonVars);

        //Craft::$app->getView()->registerJs('new Craft.MagentoProductRelationMagentoProductRelationField(' . $jsonVars . ');');

        Craft::$app->getView()->registerJs("$('#{$namespacedId}-field').MagentoProductRelationMagentoProductRelationField(" . $jsonVars . ");");

        // Render the input template
        return Craft::$app->getView()->renderTemplate(
        // 'magento-product-relation/_components/fields/MagentoProductRelationField_input',
            'magento-product-relation/_components/fields/_input',
            [
                'name' => $this->handle,
                'value' => $value,
                'selectedProducts' => $content['products'] ?? null,
                'field' => $this,
                'id' => $id,
                'namespacedId' => $namespacedId,
                'inputNamespaced'   =>  $inputNamespaced,
                'content' => $content['products'] ?? null,
                'siteParam' => MagentoProductRelation::getInstance()->getSettings()->getSiteParam(),
                'currentSite'    =>  Craft::$app->request->get(MagentoProductRelation::getInstance()->getSettings()->getSiteParam()),
            ]
        );
    }
}
