<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

namespace glue\magentoproductrelation\models;

use glue\magentoproductrelation\MagentoProductRelation;

use Craft;
use craft\base\Model;

/**
 * MagentoProductRelation Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Glue
 * @package   MagentoProductRelation
 * @since     1.0.2
 */
class Query extends Model
{
    // Public Properties
    // =========================================================================
    

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['fieldName', 'string'],
            ['fieldValue', 'string'],
            [['fieldName','fieldValue'],'required']
        ];
    }
}
