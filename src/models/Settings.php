<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

namespace glue\magentoproductrelation\models;


use Craft;
use craft\base\Model;
use glue\magentoproductrelation\MagentoProductRelation;
use glue\magentoproductrelation\services\cache\CacheStack;
use glue\magentoproductrelation\services\magento\MagentoClient;
use glue\magentoproductrelation\services\magento\rest\stores\StoreView;
use glue\magentoproductrelation\services\magento\rest\stores\StoreViewClient;
use glue\magentoproductrelation\services\validation\ValidationClient;
use GuzzleHttp\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * MagentoProductRelation Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Glue
 * @package   MagentoProductRelation
 * @since     1.0.2
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * Some field model attribute
     *
     * @var string
     */
    public $service_type = '';
    public $authToken = '';
    public $url = '';

    public $sites = [];
    public $magentoStoreViews = [];
    public $craft_site_magento_store_view = [];
    public $media_url;

    public function getSites() : array {

        $list = [];

        foreach (Craft::$app->getSites()->getAllSites() as $site){

            $list[$site['id']] = $site['name'] . '(' . $site['language'] . ')';
        }

        return $list;
    }

    public function getMagentoStoreViews() : array {

        $storeView = new StoreView();

        return $storeView->toArray();
    }

    public function getSiteParam() : string {

        return 'site';
    }


    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['service_type', 'string'],
            ['craft_site_magento_store_view','each', 'rule' => ['integer']],
            [['url', 'media_url', 'authToken'], 'string'],
            [['service_type','authToken','url'],'required'],

            // custom validations
            ['authToken', 'authorized']
        ];
    }

    public function authorized()
    {
        $client = new ValidationClient($this->url, $this->authToken);

        if($client->unauthorized()) {
            $this->addError('authToken', 'Invalid authorization token');
        }
    }
}
