<?php
/**
 * MagentoProductRelation plugin for Craft CMS 3.x
 *
 * Plugin to show content from magento into craft
 *
 * @link      https://www.glue.be
 * @copyright Copyright (c) 2019 Glue
 */

namespace glue\magentoproductrelation\twigextensions;

use glue\magentoproductrelation\services\magento\rest\ProductsBySku;
use glue\magentoproductrelation\services\magento\rest\ProductsClient;
use Twig_Function;
use glue\magentoproductrelation\services\magento\SearchService;
use Craft;

class MagentoProductRelationTwigExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        return [
            new Twig_Function('magentoContent', [$this, 'magentoContent']),
        ];
    }

    public function magentoContent($skus)
    {
        $skuArray = explode(',',$skus);
        $search = Craft::$container->get(SearchService::class);

        return $search->sku($skuArray);
    }


    // public function getFilters()
    // {
    //     return [
    //         new \Twig_SimpleFilter('someFilter', [$this, 'someInternalFunction']),
    //     ];
    // }

}
