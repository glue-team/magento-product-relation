/**
 * MagentoProductRelation plugin for Craft CMS
 *
 * MagentoProductRelationField Field JS
 *
 * @author    Glue
 * @copyright Copyright (c) 2019 Glue
 * @link      https://www.glue.be
 * @package   MagentoProductRelation
 * @since     1.0.2MagentoProductRelationMagentoProductRelationField
 */


(function ( $, window, document, undefined ) {


    var pluginName = "MagentoProductRelationMagentoProductRelationField",
        defaults = {
        };

    // Plugin constructor
    function Plugin( element, options ) {

        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;


        this.init();
    }

    Plugin.prototype = {

        init: function(id) {

            var _this = this;

            const selector = _this.options.prefix + 'my-modal-'+ _this.options.id;

            // Create modal
            const twigModal = new Garnish.Modal($('#' +  selector), {
                autoShow: false
            });
            // Open modal
            $('#' +  _this.options.prefix + 'show-modal-btn-' + _this.options.id).click(function() {

                twigModal.show();

            });
            // Close modal
            $('#' +  _this.options.prefix + 'close-modal-btn-' + _this.options.id).click(function() {
                twigModal.hide();
            });

            $('#' +  _this.options.prefix + 'close-modal-select-btn-' + _this.options.id).click(function() {
                twigModal.hide();
            });

            $(function () {
                /* -- _this.options gives us access to the $jsonVars that our FieldType passed down to us */
                window.magentoPopup(_this.options);
            });
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );
