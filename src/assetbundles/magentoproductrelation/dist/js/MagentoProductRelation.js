/**
 * MagentoProductRelation plugin for Craft CMS
 *
 * MagentoProductRelation JS
 *
 * @author    Glue
 * @copyright Copyright (c) 2019 Glue
 * @link      https://www.glue.be
 * @package   MagentoProductRelation
 * @since     1.0.2
 */