<?php
/**
 * Created by PhpStorm.
 * User: michielbogaert
 * Date: 15/01/20
 * Time: 13:59
 */

namespace glue\magentoproductrelation\controllers;

use Craft;
use craft\web\Controller;
use glue\magentoproductrelation\Exceptions\MagentoProductRelationException;
use glue\magentoproductrelation\services\magento\MagentoService;
use glue\magentoproductrelation\services\magento\SearchService;

class QueryController extends Controller
{
    // Properties
    // =========================================================================

    protected $allowAnonymous = [];

    public function init()
    {
        try {
            MagentoService::init();
        } catch(MagentoProductRelationException $e) {
            Craft::$app->session->setError("Magento Plugin: {$e->getMessage()}");
        }

        parent::init();
    }


    // Public Methods
    // =========================================================================

    public function actionIndex()
    {
        $request = Craft::$app->getRequest();

        $fieldName = $request->get('fieldName');
        $fieldValue = $request->get('fieldValue');
        $currentPage = $request->get('currentPage', 1);

        $search = Craft::$container->get(SearchService::class);

        //@todo move to implementation
        if($fieldName === 'sku'){
            $fieldValue = [$fieldValue];
        }

        $response = $search->{$fieldName}($fieldValue, [
            'current_page' => $currentPage,
        ]);

        $templates = [
            'products' => Craft::$app->getView()->renderTemplate('magento-product-relation/table', ['products' => $response['products']]),
            'pagination' => Craft::$app->getView()->renderTemplate('magento-product-relation/pagination', ['pagination' => $response['pagination']]),
        ];

        return $this->asJson(array_merge(['templates' => $templates], $response));
    }

    public function actionGraph(){

        $search = Craft::$container->get(SearchService::class);

        //$products = $search->name('cat');

        $products = $search->sku([
            'tenant',
        ]);

        var_dump($products);
        exit;
    }

}
