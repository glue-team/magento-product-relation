import path from 'path';

export default {
  entry: {
    Search: './resources/js/src/index.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './src/assetbundles/magentoproductrelationfieldfield/dist/js'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'eslint-loader'
          }
      ]
      }
    ]
  }
};
